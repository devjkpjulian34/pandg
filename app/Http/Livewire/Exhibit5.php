<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Exhibit5 extends Component
{
    public $content;
    public $vid_link;

    public function mount()
    {
        $this->content = 0;
    }

    public function changeContent($content)
    {
        $this->content = $content;

        switch ($this->content) {
            case 2:
                $this->vid_link = 'https://player.vimeo.com/video/703569612';
                break;

            case 3:
                $this->vid_link = 'https://player.vimeo.com/video/704084090';
                break;

            default:
                $this->vid_link = 'https://player.vimeo.com/video/703567848';
                break;
        }
    }

    public function render()
    {
        return view('livewire.exhibit5');
    }
}
