<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Exhibit3 extends Component
{
    public $content;
    public $vid_link = 'https://player.vimeo.com/video/703569860';

    public function mount()
    {
        $this->content = 0;
    }

    public function changeContent($content)
    {
        $this->content = $content;
    }

    public function render()
    {
        return view('livewire.exhibit3');
    }
}
