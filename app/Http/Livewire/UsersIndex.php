<?php

namespace App\Http\Livewire;

use Carbon\Carbon;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class UsersIndex extends Component
{
    use WithPagination;

    protected $response;

    public $selectedData = null;
    public $editModal = false;
    public $search = null;

    public $headers = array('PRC Number','Fullname','Email Address','Role','Date Created','Status','Actions');
    
    public function boot()
    {
        $this->resetPage();
    }

    public function mount(User $users)
    {
        $this->resetPage();
        $this->response = $users->all();
    }

    public function updating()
    {
        $this->resetPage();
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function renderRole($role)
    {
        $renderedRole = null;
        switch ($role) {
            case 0:
                $renderedRole = 'Admin';
                break;
            default:
                $renderedRole = 'User';
                break;
        }
        return $renderedRole;
    }

    public function renderStatus($status)
    {
        return !is_null($status) ? 'Verified' : 'Not Verified';
    }

    public function verifyUser($id)
    {
        $user = User::findOrFail($id);
        $user->email_verified_at = Carbon::now();
        $user->save();
    }

    public function verifyAll()
    {
        $users = User::where('email_verified_at',null)->get();

        foreach($users as $user)
        {
            $user->email_verified_at = Carbon::now();
            $user->save();
        }
    }

    public function render()
    {
        if(is_null($this->search)) {
            $this->response = User::paginate(5);
        } else {
            $this->response = User::search($this->search)->paginate(5);
        }

        return view('livewire.users-index',[
            'response' => $this->response,
        ]);
    }
}
