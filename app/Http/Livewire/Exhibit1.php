<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Exhibit1 extends Component
{
    public $content;
    public $vid_link;

    public function mount()
    {
        $this->content = 0;
        $this->vid_link = 'https://player.vimeo.com/video/703142793';
    }

    public function changeContent($content)
    {
        $this->content = $content;

        switch ($this->content) {
            case 1:
                $this->vid_link = 'https://player.vimeo.com/video/703142852';
                break;

            case 2:
                $this->vid_link = 'https://player.vimeo.com/video/703142925';
                break;

            case 3:
                $this->vid_link = 'https://player.vimeo.com/video/703143992';
                break;

            case 4:
                $this->vid_link = 'https://player.vimeo.com/video/703143716';
                break;

            case 5:
                $this->vid_link = 'https://player.vimeo.com/video/704084090';
                break;
            
            default:
                $this->vid_link = 'https://player.vimeo.com/video/703142793';
                break;
        }
    }

    public function render()
    {
        return view('livewire.exhibit1');
    }
}
