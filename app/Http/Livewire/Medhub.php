<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Medhub extends Component
{
    public $content;
    public $vid_link;

    public function mount()
    {
        $this->content = 0;
        $this->content_link = 'https://pghealthmedicalevents.com/public/pdf/Early_Intervention.pdf#toolbar=0&navpanes=0';
    }

    public function changeContent($content)
    {
        $this->content = $content;

        switch ($this->content) {
            case 1:
                $this->content_link = 'https://player.vimeo.com/video/704335293?h=6eb1e7fc36&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479';
                break;

            case 2:
                $this->content_link = 'https://pghealthmedicalevents.com/public/pdf/5stepstoPNdiagnosis.pdf#toolbar=0&navpanes=0';
                break;
            
            case 3:
                $this->content_link = 'https://pghealthmedicalevents.com/public/pdf/5stepstoPNdiagnosis.pdf#toolbar=0&navpanes=0';
                break;

            case 4:
                $this->content_link = 'https://pghealthmedicalevents.com/public/pdf/HowToDiagnosisPN.pdf#toolbar=0&navpanes=0';
                break;

            case 5:
                $this->content_link = 'https://pghealthmedicalevents.com/public/pdf/NeurotropicBVitaminsinNerveHealth.pdf#toolbar=0&navpanes=0';
                break;

            case 6:
                $this->content_link = 'https://pghealthmedicalevents.com/public/pdf/PNSymptomsandRiskGroups.pdf#toolbar=0&navpanes=0';
                break;

            case 7:
                $this->content_link = 'https://pghealthmedicalevents.com/public/img/3_4_1.jpg';
                break;

            case 8:
                $this->content_link = 'https://player.vimeo.com/video/704336026?h=cea27b6b88&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479';
                break;

            case 9:
                $this->content_link = 'https://player.vimeo.com/video/704335716?h=cea57d9e92&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479';
                break;
            
            default:
                $this->content_link = 'https://pghealthmedicalevents.com/public/pdf/Early_Intervention.pdf#toolbar=0&navpanes=0';
                break;
        }
    }

    public function render()
    {
        return view('livewire.medhub');
    }
}
