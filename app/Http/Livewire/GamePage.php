<?php

namespace App\Http\Livewire;

use App\Models\Games;
use Livewire\Component;

class GamePage extends Component
{
    protected $response;

    public $start = false;
    public $time = null;
    public $start_time = null;
    public $end_time = null;
    public $questions = [];
    public $answered = 0;
    public $previous = 0;
    public $score = 0;
    public $quiz = 0;

    public $headers = array('Fullname','Score','Time in Seconds');

    public $questionData = array(
        [
            'question' => 'Which of the following substances enhance iron absorption?',
            'answers' => array('Polyphenois in tea','Vitamin C','Vitamin A','Sodium Chloride'),
            'correct_answer' => 'Vitamin C'
        ],
        [
            'question' => 'Which of the following is a possible consequence of iron defeciency anemia in pregnant women?',
            'answers' => array('Intrauterine fetal death','Ectopic pregnancy','Pre-eclampsia','All of the above'),
            'correct_answer' => 'Pre-eclampsia'
        ],
        [
            'question' => 'Where does iron absorption occur in the body?',
            'answers' => array('Large intestine','Liver','Small intestine','Stomach'),
            'correct_answer' => 'Small intestine'
        ],
        [
            'question' => 'What virus is the most common cause of Common cold? ',
            'answers' => array('Rhinovirus','Coronavirus','Adenovirus','Respiratory Syncytial virus'),
            'correct_answer' => 'Rhinovirus'
        ],
        [
            'question' => 'Do you know how many times an average person suffers from cold in their entire lifetime? ',
            'answers' => array('100 times','1000 times','200 times','300 times'),
            'correct_answer' => '200 times'
        ],
        [
            'question' => 'The total symptom  duration of cold during person’s life is:',
            'answers' => array('1 year','2 years','>=3 years','10 years'),
            'correct_answer' => '>=3 years'
        ],
        [
            'question' => 'How much sleep do adults (18-64 y) need on average?',
            'answers' => array('8-10 hrs','7-9 hrs','7-8 hrs','No recommendation'),
            'correct_answer' => '7-9 hrs'
        ],
        [
            'question' => 'Melatonin production peaks in children and at what age (yrs) does it decrease less than half the melatonin we did as a child?',
            'answers' => array('50-60 years','20 years','After our 30s','70-80s'),
            'correct_answer' => 'After our 30s'
        ],
        [
            'question' => 'Melatonin is a hormone produced mainly by the:',
            'answers' => array('Suprachiasmic nucleus','Pineal gland','Adrenal glands','Hypothalamus'),
            'correct_answer' => 'Pineal gland'
        ],
        [
            'question' => 'What type of nerves have the fastest nerve conduction velocity?',
            'answers' => array('Unmyelinated nerves','Myelinated nerves','Damaged nerves','Long nerves'),
            'correct_answer' => 'Myelinated nerves'
        ],
        [
            'question' => 'If ____ or more of the nerve fibers are damaged, nerve regeneration is not possible anymore and the so called “point of no return” is reached',
            'answers' => array('80%','50%','65%','40%'),
            'correct_answer' => '50%'
        ],
        [
            'question' => 'What is the second most common cause of neuropathy?',
            'answers' => array('Diabetes','Idiopathic (unknown cause)','Infections','Alcohol'),
            'correct_answer' => 'Idiopathic (unknown cause)'
        ],
        [
            'question' => 'Which does not describe Vitamin B1?',
            'answers' => array('Involved in the energy metabolism','Involved in the metabolism of neurotransmitters','Fat Soluble','Involved in nerve stimulation'),
            'correct_answer' => 'Fat Soluble'
        ],
        [
            'question' => 'What does neurotropic mean',
            'answers' => array('Having an affinity to and nourishing the nervous system','Being water soluble ','Having a positive charge in the chemical structure','Being rare in the nature'),
            'correct_answer' => 'Having an affinity to and nourishing the nervous system'
        ],
        [
            'question' => 'Title of this event?',
            'answers' => array('Neuropathy?','P&G Health Medical Events','Breaking the wave','None of the Above'),
            'correct_answer' => 'Breaking the wave'
        ]
    );

    public function mount()
    {
        $random_numbers = array_rand($this->questionData,5);

        foreach($random_numbers as $rand)
        {
            array_push($this->questions,$this->questionData[$rand]);
        }
    }


    public function changeStatus($status)
    {
        $this->start = $status;

        if($this->start == true)
        {
            $this->start_time = microtime(true); 
        } else {
            $this->end_time = microtime(true);
            $this->time = intval($this->end_time - $this->start_time);
        }
    }

    public function nextQuestion()
    {
        $this->previous = $this->score;
        
        if($this->quiz < 4)
        {
            $this->quiz++;
        } else {
            
            $this->changeStatus(false);

            $player = Games::where('user_id',auth()->user()->id)->first();

            if(is_null($player))
            {
                Games::create([
                    'user_id' => auth()->user()->id,
                    'prc_number' => auth()->user()->prc_number,
                    'name' => auth()->user()->name,
                    'email' => auth()->user()->email,
                    'score' => $this->score,
                    'time' => $this->time
                ]);
            } else {
                if($player->score <= $this->score)
                {
                    $player->score = $this->score;
                    
                    if($player->time > $this->time)
                    {
                        $player->time = $this->time;
                    }

                    $player->save();
                }
            }

            return redirect()->route('games');
        }
    }

    public function answerQuestion($answer)
    {
        if($this->questions[$this->quiz]['correct_answer'] == $answer)
        {
            $this->score++;
        }

        if($this->answered <= 4)
        {
            $this->answered++;
        }
    }

    public function render()
    {
        $this->response = Games::orderByDesc('score')->take(5);
        $this->response = $this->response->orderBy('time')->get();

        return view('livewire.game-page',[
            'response' => $this->response,
        ]);
    }
}
