<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Exhibit4 extends Component
{
    public function render()
    {
        return view('livewire.exhibit4');
    }
}
