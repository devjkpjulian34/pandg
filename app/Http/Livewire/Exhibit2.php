<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Exhibit2 extends Component
{
    public $content;
    public $vid_link = 'https://player.vimeo.com/video/703142543';

    public function mount()
    {
        $this->content = 0;
    }

    public function changeContent($content)
    {
        $this->content = $content;

        switch ($this->content) {
            case 2:
                $this->vid_link = 'https://player.vimeo.com/video/703142654';
                break;
                
            default:
                $this->vid_link = 'https://player.vimeo.com/video/703142543';
                break;
        }
    }

    public function render()
    {
        return view('livewire.exhibit2');
    }
}
