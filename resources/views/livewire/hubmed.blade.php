<div>
    <div class="flex flex-row w-screen min-h-screen pt-24 antialiased text-gray-800">
        <div class="flex flex-col items-center justify-center w-full h-auto py-12 space-y-32 overflow-y-auto text-center bg-white scrollbar-hide scroll-smooth">
            <div id="hub1" class="w-full h-auto space-y-2">
                <h1 class="py-2 text-xl font-extrabold underline md:text-3xl text-lime-400">COUGH & COLD</h1>
                <iframe src="https://pghealthmedicalevents.com/public/pdf/Early_Intervention.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
            </div>
            <div id="hub2" class="w-full h-auto space-y-2">
                <h1 class="py-2 text-xl font-extrabold underline md:text-3xl text-lime-400">IRON DEFICIENCY ANEMIA</h1>
                <div class="w-full h-full aspect-video">
                    <video class="w-full h-full" controls>
                        <source src="{{asset('vid/blood_health.mp4')}}" type="video/mp4">
                    </video>
                </div>
            </div>
            <div id="hub3" class="w-full h-auto space-y-2">
                <h1 class="py-4 text-xl font-extrabold underline md:text-3xl text-lime-400">NERVE CARE</h1>
                <div id="hub3_1" class="flex flex-col w-full h-auto space-y-2">
                    <h1 class="py-4 font-extrabold text-gray-600 capitalize text-md md:text-xl">5 Steps to PN Diagnosis</h1>
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/5stepstoPNdiagnosis.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div id="hub3_2" class="flex flex-col w-full h-auto space-y-2">
                    <h1 class="py-4 font-extrabold text-gray-600 capitalize text-md md:text-xl">How to Diagnosis PN</h1>
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/HowToDiagnosisPN.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div id="hub3_3" class="flex flex-col w-full h-auto space-y-2">
                    <h1 class="py-4 font-extrabold text-gray-600 capitalize text-md md:text-xl">Neurotropic B Vitamins in Nerve Health</h1>
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/NeurotropicBVitaminsinNerveHealth.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div id="hub3_4" class="flex flex-col w-full h-auto space-y-2">
                    <h1 class="py-4 font-extrabold text-gray-600 capitalize text-md md:text-xl">PN Symptoms Management</h1>
                    <img src="{{asset('img/3_4_1.jpg')}}" class="w-full h-full">
                </div>
                <div id="hub3_5" class="flex flex-col w-full h-auto space-y-2">
                    <h1 class="py-4 font-extrabold text-gray-600 capitalize text-md md:text-xl">PN Symptoms and Risk Groups</h1>
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/PNSymptomsandRiskGroups.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div id="hub3_6" class="flex flex-col w-full h-auto space-y-2">
                    <h1 class="py-4 font-extrabold text-gray-600 capitalize text-md md:text-xl">Neuropathy Video</h1>
                    <div class="w-full h-full py-6 aspect-video">
                        <video class="w-full h-full" controls>
                            <source src="{{asset('vid/hub_3_vid1.mp4')}}" type="video/mp4">
                        </video>
                    </div>
                </div>
                <div id="hub3_7" class="flex flex-col w-full h-auto space-y-2">
                    <h1 class="py-4 font-extrabold text-gray-600 capitalize text-md md:text-xl">Steps to Diagnose Video</h1>
                    <div class="w-full h-full py-6 aspect-video">
                        <video class="w-full h-full" controls>
                            <source src="{{asset('vid/hub_3_vid2.mp4')}}" type="video/mp4">
                        </video>
                    </div>
                </div>
                <div class="block w-full h-auto space-y-6 md:hidden">
                    <a href="{{asset('img/neuropathy_awareness.jpg')}}" class="px-4 py-2 text-sm font-extrabold text-teal-500 border-2 border-teal-500 rounded-full hover:text-gray-600 hover:border-gray-600" download>DOWNLOAD CONTENT...</a>
                </div>
            </div>
        </div>
    </div>
</div>
