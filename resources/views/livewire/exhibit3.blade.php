<div>
    <div class="flex flex-row w-screen h-screen bg-white/50">
        <div class="hidden w-1/6 min-h-screen py-32 bg-white border-r-4 border-lime-400 md:block">
            <ul class="flex flex-col w-full space-y-6">
                <li class="flex items-center justify-center w-full px-6">
                    <img id="logo" src="https://pghealthmedicalevents.com/public/img/vicks.png" class="w-auto h-20">
                </li>
                <li class="flex items-center justify-center w-full">
                    <ul class="space-y-2 list-none">
                        <li>
                            <button wire:click="changeContent(0)" type="button" class="flex flex-row items-center justify-center focus:outline-none {{$content == 0 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">VFD / VID</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(1)" type="button" class="flex flex-row items-center justify-center focus:outline-none  {{$content == 1 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">VICKS BRAND VIDEO</span>
                            </button>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="hidden w-screen h-screen pt-24 bg-white/90 md:w-5/6 md:block">
            @if($content == 0)
                <div class="w-full h-full">
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/vfd.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
            @else
                <div class="flex items-center justify-center w-full h-full">
                    <div class="w-full h-full aspect-video">
                        <iframe class="w-full h-full" src="{{$vid_link}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            @endif
        </div>
        <div class="block w-screen h-full pt-32 space-y-2 bg-white/90 md:hidden">
            <div class="flex flex-col w-full h-full p-6 space-y-2">
                <img id="logo" src="https://pghealthmedicalevents.com/public/img/vicks.png" class="w-auto h-20">
                <iframe src="https://pghealthmedicalevents.com/public/pdf/vfd.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
            </div>
            <div class="w-full h-full p-6">
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="{{$vid_link}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
