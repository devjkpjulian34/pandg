<div>
    <div class="flex flex-col p-6 bg-white border-b border-gray-200 sm:px-20">
        <div class="flex flex-row w-full space-x-2 border-b border-gray-500">
            <div class="flex w-full py-2 shadow-sm">
                <span class="inline-flex items-center px-3 text-sm text-gray-500 border border-r-0 border-gray-300 rounded-l-xl bg-gray-50">
                    <i class="fa-solid fa-magnifying-glass"></i>
                </span>
                <input wire:model="search" type="text" class="flex-1 block w-full border-gray-300 rounded-none focus:ring-teal-500 focus:border-teal-500 rounded-r-xl sm:text-sm" placeholder="Search something here...">
            </div>
            <div class="flex items-center justify-center w-auto">
                <button wire:click="verifyAll" type="button" class="px-2 py-1 border-2 border-gray-600 rounded-xl">
                    <i class="fa-solid fa-circle-check"></i>
                </button>
            </div>
            <div class="flex items-center justify-center w-auto">
                <a href="{{route('users.index')}}" class="px-2 py-1 border-2 border-gray-600 rounded-xl">
                    <i class="fa-solid fa-download"></i>
                </a>
            </div>
        </div>
        <div class="relative z-0 w-full my-6 overflow-x-auto shadow-md">
            <table class="w-full text-sm text-left text-gray-500 uppercase">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        @foreach($headers as $content)
                            <th scope="col" class="px-6 py-3">
                                {{$content}}
                            </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($response as $data)
                        <tr class="bg-white border-b hover:bg-lime-100">
                            <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                                {{$data->prc_number}}
                            </th>
                            <td class="px-6 py-4">
                                {{$data->name}}
                            </td>
                            <td class="px-6 py-4">
                                {{$data->email}}
                            </td>
                            <td class="px-6 py-4">
                                {{$this->renderRole($data->role)}}
                            </td>
                            <td class="px-6 py-4">
                                {{$data->created_at}}
                            </td>
                            <td class="px-6 py-4">
                                {{$this->renderStatus($data->email_verified_at)}}
                            </td>
                            <td class="flex flex-row px-6 py-4 space-x-2 text-center">
                                @if(is_null($data->email_verified_at))
                                <button wire:click="verifyUser({{$data->id}})" type="button">
                                    <i class="text-gray-700 fa-solid fa-thumbs-up"></i>
                                </button>
                                @endif
                                @if($data->id > 2)
                                <form method="POST" action="{{ route('users.destroy',$data->id) }}">
                                    @method('delete')
                                    @csrf
                                    <button type="submit">
                                        <i class="text-red-700 fa-solid fa-trash"></i>
                                    </button>
                                </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="w-full h-auto py-2 border-t-2 border-gray-300">
            {{$response->links()}}
        </div>
    </div>
</div>
