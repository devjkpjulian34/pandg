<div>
    <div class="flex flex-row w-screen h-screen bg-white/50">
        <div class="hidden w-1/4 min-h-screen py-32 bg-white border-r-4 border-lime-400 md:block">
            <ul class="flex flex-col w-full py-32 space-y-2">
                <li class="w-full">
                    <button wire:click="changeContent(0)" type="button" class="w-full flex flex-row items-center pr-6 h-11 focus:outline-none {{$content == 0 ? 'bg-gray-100 text-gray-800 border-l-4 border-transparent border-lime-500' : 'text-lime-400'}}">
                        <span class="ml-6 text-lg font-extrabold tracking-wide uppercase truncate">Cough & Cold</span>
                    </button>
                </li>
                <li class="w-full">
                    <button wire:click="changeContent(1)" type="button" class="w-full flex flex-row items-center pr-6 h-11 focus:outline-none {{$content == 1 ? 'bg-gray-100 text-gray-800 border-l-4 border-transparent border-lime-500' : 'text-lime-400'}}">
                        <span class="ml-6 text-lg font-extrabold tracking-wide uppercase truncate">Iron Deficiency Anemia</span>
                    </button>
                </li>
                <li class="w-full">
                    <button wire:click="changeContent(2)" type="button" class="w-full flex flex-row items-center pr-6 h-11 focus:outline-none {{$content >= 2 ? 'bg-gray-100 text-gray-800 border-l-4 border-transparent border-lime-500' : 'text-lime-400'}}">
                        <span class="ml-6 text-lg font-extrabold tracking-wide uppercase truncate">Nerve Care</span>
                    </button>
                </li>
                <li class="w-full {{$content >= 2 ? 'block' : 'hidden'}}">
                    <ul class="space-y-2 list-none">
                        <li>
                            <button wire:click="changeContent(3)" type="button" class="flex flex-row items-center text-gray-600 focus:outline-none hover:text-lime-400">
                                <span class="ml-6 text-xs font-extrabold tracking-wide capitalize truncate">5 Steps to PN Diagnosis</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(4)" type="button" class="flex flex-row items-center text-gray-600 focus:outline-none hover:text-lime-400">
                                <span class="ml-6 text-xs font-extrabold tracking-wide capitalize truncate">How to Diagnosis PN</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(5)" type="button" class="flex flex-row items-center text-gray-600 focus:outline-none hover:text-lime-400">
                                <span class="ml-6 text-xs font-extrabold tracking-wide capitalize truncate">Neurotropic B Vitamins in Nerve Health</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(6)" type="button" class="flex flex-row items-center text-gray-600 focus:outline-none hover:text-lime-400">
                                <span class="ml-6 text-xs font-extrabold tracking-wide capitalize truncate">PN Symptoms and Risk Groups</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(7)" type="button" class="flex flex-row items-center text-gray-600 focus:outline-none hover:text-lime-400">
                                <span class="ml-6 text-xs font-extrabold tracking-wide capitalize truncate">PN Symptoms Management</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(8)" type="button" class="flex flex-row items-center text-gray-600 focus:outline-none hover:text-lime-400">
                                <span class="ml-6 text-xs font-extrabold tracking-wide capitalize truncate">Neuropathy Video</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(9)" type="button" class="flex flex-row items-center text-gray-600 focus:outline-none hover:text-lime-400">
                                <span class="ml-6 text-xs font-extrabold tracking-wide capitalize truncate">Steps to Diagnose PN Video</span>
                            </button>
                        </li>
                        <li>
                            <a href="{{asset('img/neuropathy_awareness.png')}}" type="button" class="flex flex-row items-center text-teal-500 hover:text-lime-600 focus:outline-none" download>
                                <span class="ml-6 text-sm font-extrabold tracking-wide uppercase truncate">Downloadable Content...</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="hidden w-screen h-screen pt-24 bg-white/90 md:w-3/4 md:block">
            @if($content == 1)
                <div class="flex items-center justify-center w-full h-full">
                    <div class="w-full h-full aspect-video">
                        <iframe class="w-full h-full" src="{{$content_link}}" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            @elseif($content == 7)
                <div class="w-full h-full">
                    <img src="{{$content_link}}" class="w-full h-full">
                </div>
            @elseif($content == 8)
                <div class="flex items-center justify-center w-full h-full">
                    <div class="w-full h-full aspect-video">
                        <iframe class="w-full h-full" src="{{$content_link}}" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            @elseif($content == 9)
                <div class="flex items-center justify-center w-full h-full">
                    <div class="w-full h-full aspect-video">
                        <iframe class="w-full h-full" src="{{$content_link}}" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            @else
                <div class="w-full h-full">
                    <iframe src="{{$content_link}}" class="w-full h-full"></iframe>
                </div>
            @endif
        </div>
        <div class="block w-screen min-h-screen px-6 pb-4 overflow-y-auto pt-28 bg-white/90 md:hidden">
            <div class="flex flex-col items-center w-full h-full p-2 space-y-6">
                <h1 class="py-2 text-xl font-extrabold underline md:text-3xl text-lime-400">Cough & Cold</h1>
                <div class="flex flex-col w-full h-full space-y-2">
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/Early_Intervention.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <h1 class="py-2 text-xl font-extrabold underline md:text-3xl text-lime-400">Iron Deficiency Anemia</h1>
                <div class="flex flex-col w-full h-auto space-y-2 aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/704335293?h=6eb1e7fc36&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=5847" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <h1 class="py-2 text-xl font-extrabold underline md:text-3xl text-lime-400">Nerve Care</h1>
                <div class="flex flex-col w-full h-full space-y-2">
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/5stepstoPNdiagnosis.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div class="flex flex-col w-full h-full space-y-2">
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/HowToDiagnosisPN.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div class="flex flex-col w-full h-full space-y-2">
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/NeurotropicBVitaminsinNerveHealth.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div class="flex flex-col w-full h-full space-y-2">
                    <iframe src="https://pghealthmedicalevents.com/public/pdf/PNSymptomsandRiskGroups.pdf#toolbar=0&navpanes=0" class="w-full h-full"></iframe>
                </div>
                <div class="flex flex-col w-full h-auto space-y-2">
                    <img src="{{asset('img/3_4_1.jpg')}}" class="w-full h-full">
                </div>
                <div class="flex flex-col w-full h-auto space-y-2 aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/704336026?h=cea27b6b88&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="flex flex-col w-full h-auto space-y-2 aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/704335716?h=cea57d9e92&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="flex flex-col w-full h-auto pb-6 space-y-2">
                    <a href="{{asset('img/neuropathy_awareness.png')}}" class="px-4 py-2 text-sm font-extrabold text-center text-teal-500 border-2 border-teal-500 rounded-full hover:text-gray-600 hover:border-gray-600" download>DOWNLOAD CONTENT...</a>
                </div>
            </div>
        </div>
    </div>
</div>
