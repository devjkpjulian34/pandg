<div>
    @if($start == false)
    <div class="flex flex-col items-center justify-center p-6 space-y-6 bg-white border-4 border-white md:border-lime-400 sm:px-20">
        <h1 class="text-4xl font-extrabold text-lime-400">TOP 5 SCORERS</h1>
        <table class="w-full text-sm text-left text-gray-500">
            <thead class="text-xs font-extrabold text-gray-800 uppercase bg-gray-50">
                <tr>
                    @foreach($headers as $content)
                        <th scope="col" class="px-6 py-3">
                            {{$content}}
                        </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($response as $data)
                <tr class="bg-white border-b hover:bg-teal-100">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 uppercase whitespace-nowrap">
                        {{$data->name}}
                    </th>
                    <td class="px-6 py-4 text-center">
                        {{$data->score}}
                    </td>
                    <td class="px-6 py-4 text-center">
                        {{$data->time}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!-- <button wire:click="changeStatus(true)" type="button" class="px-6 py-2 text-2xl font-extrabold text-white transform border-2 bg-lime-400 border-lime-600 rounded-xl hover:scale-105">
            START GAME
        </button> -->
    </div>
    @else
    <div class="p-6 space-y-6 bg-white border-4 border-white md:border-lime-400 sm:px-20">
        <div class="flex flex-row items-center justify-between">
            <h1 class="text-4xl font-extrabold text-lime-400">QUIZ {{$quiz + 1}}</h1>
            <h1 class="text-4xl font-extrabold text-gray-800">SCORE : {{$score}}</h1>
        </div>
        <div class="flex flex-col space-y-6">
            <h2 class="text-3xl font-extrabold text-center text-gray-800">{{$questions[$quiz]['question']}}  {{$answered > $quiz ? ($score != $previous ? '(CORRECT)' : '') : ''}}</h2>
            <div class="grid items-center justify-center w-full grid-cols-1 gap-6 md:grid-cols-2">
                @foreach($questions[$quiz]['answers'] as $answer)
                    <button wire:click="answerQuestion('{{$answer}}')" type="button" class="h-auto px-6 py-2 text-2xl font-extrabold transform bg-white border-2 text-lime-400 border-lime-400 rounded-xl hover:scale-105 disabled:scale-100 disabled:bg-gray-300 disabled:text-gray-800 disabled:border-gray-600" {{$answered > $quiz ? 'disabled' : ''}}>
                        {{$answer}}
                    </button>
                @endforeach
            </div>
        </div>
        <div class="flex flex-row items-end justify-end">
            @if($quiz <= 4)
            <button wire:click="nextQuestion" type="button" class="px-6 py-2 text-2xl font-extrabold transform bg-lime-400 border-2 text-white border-lime-600 rounded-xl hover:scale-105 {{$answered <= $quiz ? 'hidden' : ''}}">
                {{$quiz < 4 ? 'NEXT QUESTION' : 'END GAME'}}
            </button>
            @endif
        </div>
    </div>
    @endif
</div>
