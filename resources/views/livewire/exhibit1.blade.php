<div>
    <div class="flex flex-row w-screen h-screen bg-transparent md:bg-white/50">
        <div class="hidden w-1/6 min-h-screen py-32 bg-white border-r-4 border-lime-400 md:block">
            <ul class="flex flex-col w-full space-y-6">
                <li class="flex items-center justify-center w-full px-6">
                    <img id="logo" src="https://pghealthmedicalevents.com/public/img/sangobionplus.png" class="w-auto h-20">
                </li>
                <li class="flex items-center justify-center w-full">
                    <ul class="space-y-2 list-none">
                        <li>
                            <button wire:click="changeContent(0)" type="button" class="flex flex-row items-center justify-center focus:outline-none {{$content == 0 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">SANGOBION IRON + for ADULT</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(1)" type="button" class="flex flex-row items-center justify-center focus:outline-none  {{$content == 1 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">SANGOBION IRON + for ELDERLY</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(2)" type="button" class="flex flex-row items-center justify-center focus:outline-none {{$content == 2 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">SANGOBION IRON + for OB PREGNANT</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(5)" type="button" class="flex flex-row items-center justify-center focus:outline-none  {{$content == 2 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">SANGOBION ADOLESCENT VIDEO</span>
                            </button>
                        </li>
                    </ul>
                </li>
                <li class="flex items-center justify-center w-full px-6">
                    <img id="logo" src="https://pghealthmedicalevents.com/public/img/sangobionforte.png" class="w-auto h-20">
                </li>
                <li class="flex items-center justify-center w-full">
                    <ul class="space-y-2 list-none">
                        <li>
                            <button wire:click="changeContent(3)" type="button" class="flex flex-row items-center justify-center focus:outline-none {{$content == 3 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">SANGOBION FORTE for OB PREGNANT</span>
                            </button>
                        </li>
                        <li>
                            <button wire:click="changeContent(4)" type="button" class="flex flex-row items-center justify-center focus:outline-none {{$content == 4 ? 'text-lime-400 hover:text-gray-600' : 'text-gray-600 hover:text-lime-400'}}">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">SANGOBION FORTE for ANEMIA</span>
                            </button>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="hidden w-screen h-screen pt-24 bg-white/90 md:w-5/6 md:block">
            <div class="flex items-center justify-center w-full h-full">
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="{{$vid_link}}" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="block w-screen min-h-screen px-6 pb-4 overflow-y-auto pt-28 bg-white/90 md:hidden">
            <div class="flex flex-col w-full h-full space-y-6">
                <img id="logo" src="https://pghealthmedicalevents.com/public/img/sangobionplus.png" class="w-auto h-20">
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/703142793" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/703142852" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/703142925" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <img id="logo" src="https://pghealthmedicalevents.com/public/img/sangobionforte.png" class="w-auto h-20">
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/703143992" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/703143716" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/704084090" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
