<div>
    <div class="flex flex-row w-screen h-screen bg-white/50">
        <div class="hidden w-1/6 min-h-screen py-32 bg-white border-r-4 border-lime-400 md:block">
            <ul class="flex flex-col w-full space-y-6">
                <li class="flex items-center justify-center w-full px-6">
                    <img id="logo" src="https://pghealthmedicalevents.com/public/img/zzquillV.png" class="w-auto h-20">
                </li>
                <li class="flex items-center justify-center w-full">
                    <ul class="space-y-2 list-none">
                        <li>
                            <button type="button" class="flex flex-row items-center justify-center focus:outline-none text-lime-400 hover:text-gray-600">
                                <span class="text-xs font-extrabold tracking-wide capitalize truncate">VICKS ZZZQUIL NATURA SLEEP VIDEO</span>
                            </button>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="hidden w-screen h-screen pt-24 bg-white/90 md:w-5/6 md:block">
            <div class="flex items-center justify-center w-full h-full">
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/703144124" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="block w-full min-h-screen pt-32 overflow-y-auto bg-white/90 md:hidden">
            <div class="flex flex-col items-center w-full h-full p-6">
                <img id="logo" src="https://pghealthmedicalevents.com/public/img/zzquillV.png" class="w-auto h-16">
                <div class="w-full h-full aspect-video">
                    <iframe class="w-full h-full" src="https://player.vimeo.com/video/703144124" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
