<x-app-layout>
    <div class="flex items-center justify-center w-screen min-h-screen px-2 pb-0 md:px-32 md:pb-4 pt-28">
        <img class="relative z-10 w-full h-full md:w-11/12 md:h-11/12" src="{{asset('img/schedule.jpg')}}">
    </div>
</x-app-layout>