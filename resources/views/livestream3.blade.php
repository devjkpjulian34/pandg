<x-app-layout>
    {{-- @php
        date_default_timezone_set('Asia/Manila');
    @endphp
    @if(date("Y/m/d h:i:s A") >= '2022/04/29 04:45:00 PM' || auth()->user()->id == 2)
        <div class="flex flex-col items-center justify-center w-screen h-screen px-0 pb-4 overflow-hidden md:px-32 md:flex-row pt-28 bg-white/50">
            <div class="w-full h-full bg-white md:w-2/3 aspect-video">
                <iframe class="w-full h-full" src="https://vimeo.com/event/2054293/embed" title="Livestream" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="flex flex-col w-full h-full bg-white md:w-1/3">
                <iframe class="w-full h-full" src="https://vimeo.com/event/2054293/chat/interaction" title="Chat" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="flex items-center justify-center w-full p-2 bg-white border-2 border-gray-300">
                    <a href="https://forms.office.com/r/ZwpVFybHi4" class="font-extrabold text-center text-gray-600 uppercase focus:outline-none hover:text-lime-800">
                        Click Here to Answer The Post-Evaluation
                    </a>
                </div>
            </div>
        </div>
    @else
        <div class="flex flex-col items-center justify-center w-screen h-screen space-y-6 overflow-hidden bg-white/50">
            <h2 class="font-extrabold text-md md:text-xl">EVENT WILL START IN:</h2>
            <h1 id="timer" class="text-2xl font-extrabold md:text-6xl text-lime-600"></h1>
            <p class="text-xs text-center md:text-xl">Note: Please refresh this page if it does not reload automatically.</p>
        </div>
        <script>
            // Set the date we're counting down to
            var countDownDate = new Date("Apr 29, 2022 16:45:00").getTime();
                
                // Update the count down every 1 second
            var x = setInterval(function() {
            
                // Get today's date and time
                var now = new Date().getTime();
            
                // Find the distance between now and the count down date
                var distance = countDownDate - now;
            
                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
                // Display the result in the element with id="demo"
                document.getElementById("timer").innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";
            
                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("timer").innerHTML = "Event Starting...";
                    window.location.reload();
                }
            }, 1000);
        </script>
    @endif --}}
    <div class="flex flex-col items-center justify-center w-screen h-screen space-y-6 overflow-hidden bg-white/50">
        <h2 class="font-extrabold text-md md:text-xl">Catch the show again starting tomorrow as we will be uploading it on this page. Thank you for joining us and see you again soon!</h2>
    </div>
</x-app-layout>