<x-app-layout>
    @if(auth()->user()->role == 0)
        <div class="py-32">
            <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
                <div class="overflow-hidden bg-white shadow-xl sm:rounded-lg">
                    @livewire('users-index')
                </div>
            </div>
        </div>
    @else
        <x-jet-welcome />
    @endif
</x-app-layout>
