<x-app-layout>
    <div class="flex flex-col items-start justify-start w-screen h-screen px-6 pb-4 overflow-y-auto bg-white pt-28">
        @if(auth()->check() && auth()->user()->role == 0)
            @foreach($emails as $email)
                <p class="text-lime-600">{{$email}}</p>
            @endforeach
        @endif
    </div>
</x-app-layout>