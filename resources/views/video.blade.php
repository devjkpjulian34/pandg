<x-app-layout>
    <div class="flex flex-col items-center justify-center w-screen h-screen px-0 pb-4 overflow-hidden md:px-32 md:flex-row pt-28 bg-white/50">
        <div class="w-full h-full bg-white md:w-2/3 aspect-video">
            <iframe class="w-full h-full" src="https://vimeo.com/event/2071852/embed/ce4f6df0e7" title="Livestream" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="w-full h-full bg-white md:w-1/3">
            <iframe class="w-full h-full" src="https://vimeo.com/event/2071852/chat/interaction" title="Chat" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</x-app-layout>