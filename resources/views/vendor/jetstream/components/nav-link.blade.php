@props(['active'])

@php
$classes = ($active ?? false)
            ? 'uppercase bg-lime-500 inline-flex items-center px-4 py-2 leading-5 text-sm font-extrabold text-white hover:text-[#153EA8] focus:outline-none transition border-2 border-white rounded-full'
            : 'uppercase bg-[#153EA8] inline-flex items-center px-4 py-2 leading-5 text-sm font-extrabold text-white hover:text-lime-400 focus:outline-none focus:text-lime-400 transition border-2 border-white rounded-full';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>