@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-extrabold text-sm text-white uppercase']) }}>
    {{ $value ?? $slot }}
</label>
