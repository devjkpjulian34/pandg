<div class="hidden md:block">
    <img id="image-map" usemap="#image-map" class="relative z-10 w-screen h-screen" src="{{asset('img/bg.jpg')}}">

    <map name="image-map">
        {{-- <area href="{{route('video')}}" alt="Video" title="Video" coords="730,231,1197,502" shape="rect"> --}}
        <area href="{{route('livestream1')}}" alt="Session Hall 1" title="Session Hall 1" coords="557,514,555,631,120,658,116,514" shape="poly">
        <area href="{{route('livestream2')}}" alt="Session Hall 2" title="Session Hall 2" coords="1356,517,1807,519,1807,665,1363,636" shape="poly">
        <area href="{{route('medhub')}}" alt="Medical Education Hub" title="Medical Education Hub" coords="701,519,1210,517,1212,629,706,631" shape="poly">
        <area href="{{route('games')}}" alt="Trivia Game" title="Trivia Game" coords="1485,264,1488,342,1741,320,1738,232" shape="poly">
        <area href="{{route('exhibit')}}" alt="Exhibit Hall" title="Exhibit Hall" coords="165,234,165,315,440,344,444,263" shape="poly">
        <area href="{{route('cme')}}" alt="CME" title="CME" coords="1495,658,1492,814,1270,922,1668,990,1856,931,1826,683" shape="poly">
    </map>

    <script language="javascript">
        $(document).ready(function() {
            $('map').imageMapResize();
        });
    </script>
</div>

<div class="block pt-32 md:hidden">
    {{-- <div class="flex items-center justify-center w-full h-full overflow-hidden">
        <div class="w-full h-full mx-6 bg-white aspect-video">
            <iframe class="w-full h-full" src="https://vimeo.com/event/2031843/embed/a795a7875d" title="Livestream" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div> --}}
    <div class="grid items-center justify-center w-full h-full grid-cols-1 gap-2 p-6">
        <a class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105" href="{{route('medhub')}}">
            MEDICAL EDUCATION HUB
        </a>
        <a class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105" href="{{route('cme')}}">
            CME
        </a>
        <a class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105" href="{{route('livestream1')}}">
            SESSION HALL 1
        </a>
        <a class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105" href="{{route('livestream2')}}">
            SESSION HALL 2
        </a>
        <a class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105" href="{{route('exhibit')}}">
            EXHIBIT HALL
        </a>
        <a class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105" href="{{route('games')}}">
            TRIVIA GAME
        </a>
    </div>
</div>