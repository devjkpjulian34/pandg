<div class="flex flex-col items-center justify-center min-h-screen pb-32 bg-center md:bg-cover" style="background-image: url('{{asset('img/bg-guests.png')}}')">
    <div class="flex items-center justify-center w-full px-6 py-4 overflow-hidden bg-white shadow-md sm:max-w-md sm:rounded-t-xl">
        {{ $logo }}
    </div>
    <div class="w-full p-6 overflow-hidden bg-blue-700 shadow-md sm:max-w-md sm:rounded-b-xl">
        {{ $slot }}
    </div>
</div>
