<div class="flex flex-col items-center justify-center h-screen bg-center md:bg-cover" style="background-image: url('{{asset('img/bg-entrance.png')}}')">
    <div class="flex items-center justify-center w-full px-6 py-4 overflow-hidden bg-white shadow-md sm:max-w-md sm:rounded-t-xl">
        <x-jet-authentication-card-logo />
    </div>
    <div class="w-full p-6 overflow-hidden bg-blue-700 shadow-md sm:max-w-md sm:rounded-b-xl">
        <div class="mb-4 text-xl text-center text-white">
            {{ __('Thank you for registering! We look forward to having you on April 28 and 29!') }}
        </div>
    </div>
</div>