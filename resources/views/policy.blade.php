<x-guest-layout>
    <div class="flex flex-col items-center justify-center w-screen h-screen pt-4 bg-cover" style="background-image: url('{{asset('img/bg-games.png')}}')">
        <div class="flex flex-col items-center justify-center w-full p-6 mt-6 overflow-hidden prose bg-white shadow-md text-cenrter sm:max-w-2xl sm:rounded-lg">
            <div>
                <x-jet-authentication-card-logo />
            </div>
            {!! $policy !!}
        </div>
    </div>
</x-guest-layout>
