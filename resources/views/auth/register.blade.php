<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label for="fistname" value="{{ __('First Name') }}" />
                <x-jet-input id="firstname" class="block w-full mt-1" type="text" name="firstname" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="lastname" value="{{ __('Last Name') }}" />
                <x-jet-input id="lastname" class="block w-full mt-1" type="text" name="lastname" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="prc_number" value="{{ __('PRC Number') }}" />
                <x-jet-input id="prc_number" class="block w-full mt-1" type="text" name="prc_number" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="hospital" value="{{ __('Hospital Affiliation') }}" />
                <x-jet-input id="hospital" class="block w-full mt-1" type="text" name="hospital" autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block w-full mt-1" type="email" name="email" required autofocus />
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <div class="mt-4">
                    <x-jet-label for="terms">
                        <div class="flex items-center">
                            <x-jet-checkbox name="terms" id="terms"/>

                            <div class="ml-2">
                                I Agree to the <a href="{{route('policy.show')}}" target="_blank" class="text-xs underline md:text-sm text-lime-400 hover:text-white">Privacy and Consent Policies.</a>
                            </div>
                        </div>
                    </x-jet-label>
                </div>
            @endif
            
            <div class="flex items-center justify-between">
                <div class="relative w-32 h-32">
                    <img src="{{asset('img/logo1.png')}}" class="absolute bottom-0 left-0 w-24 h-24">
                </div>
                <div class="flex flex-col items-end justify-end mt-4">
                    <div class="flex flex-row justify-end mt-4">
                        <x-jet-button class="ml-4">
                            {{ __('Register') }}
                        </x-jet-button>
                    </div>
                    <div class="mt-4">
                        <a class="inline-flex items-center justify-end text-xs text-white md:text-sm" href="{{ route('login') }}">
                            {{ __('Already registered?') }} 
                            <p class="ml-2 text-lg font-extrabold uppercase md:ml-4 md:text-xl text-lime-400">
                                {{ __('Login') }}
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
