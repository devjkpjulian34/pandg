<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 text-sm font-medium text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block w-full mt-1" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('PRC Number') }}" />
                <x-jet-input id="password" class="block w-full mt-1" type="password" name="password" required autocomplete="current-password" />
            </div>

            {{-- <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div> --}}

            <div class="flex items-center justify-between">
                <div class="relative w-32 h-32">
                    <img src="{{asset('img/logo1.png')}}" class="absolute bottom-0 left-0 w-24 h-24">
                </div>
                <div class="flex flex-col items-end justify-end mt-4">
                    <div class="flex flex-row justify-end mt-4">
                        <x-jet-button class="ml-4">
                            {{ __('Login') }}
                        </x-jet-button>
                    </div>
                    <div class="mt-4">
                        <a class="inline-flex items-center justify-end text-xs text-white md:text-sm" href="{{ route('register') }}">
                            {{ __('Not yet registered?') }} 
                            <p class="ml-2 text-lg font-extrabold uppercase md:ml-4 md:text-xl text-lime-400">
                                {{ __('Register') }}
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
