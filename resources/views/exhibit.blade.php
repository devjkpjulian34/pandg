<x-app-layout>
    <div class="hidden md:block">
        <img id="image-map" usemap="#image-map" class="relative z-10 w-screen h-screen" src="{{asset('img/bg_exhibit.jpg')}}">

        <map name="image-map">
            <area href="{{route('exhibit3')}}" alt="VICKS FIRST DEFENCE/VICKS IMMUND DEFENCE" title="VICKS FIRST DEFENCE/VICKS IMMUND DEFENCE" coords="294,637,523,766" shape="rect">
            <area href="{{route('exhibit1')}}" alt="SANGOBION IRON+ / SANGOBION FORTE" title="SANGOBION IRON+ / SANGOBION FORTE" coords="578,902,786,1017" shape="rect">
            <area href="{{route('exhibit2')}}" alt="NEUROBION" title="NEUROBION" coords="1113,887,1319,1016" shape="rect">
            <area href="{{route('exhibit4')}}" alt="ZZZQUILL NATURA SLEEP" title="ZZZQUILL NATURA SLEEP" coords="1366,659,1580,774" shape="rect">
            <area href="{{route('exhibit5')}}" alt="SANGOBION KIDS/BABY" title="SANGOBION KIDS/BABY" coords="857,624,1049,741" shape="rect">
        </map>
    
        <script language="javascript">
            $(document).ready(function() {
                $('map').imageMapResize();
            });
        </script>
    </div>
    
    <div class="block pt-32 md:hidden">
        <div class="grid items-center justify-center w-full h-full grid-cols-1 gap-2 p-6">
            <a href="{{route('exhibit3')}}" class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105">
                VICKS FIRST DEFENCE/VICKS IMMUND DEFENCE
            </a>
            <a href="{{route('exhibit4')}}" class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105">
                ZZZQUILL NATURA SLEEP
            </a>
            <a href="{{route('exhibit2')}}" class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105">
                NEUROBION
            </a>
            <a href="{{route('exhibit1')}}" class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105">
                SANGOBION IRON+ / SANGOBION FORTE
            </a>
            {{-- <a href="{{route('exhibit5')}}" class="flex items-center justify-center w-full h-20 text-center text-white transform border-2 border-white bg-lime-500 rounded-xl hover:scale-105">
                SANGOBION KIDS/BABY
            </a> --}}
        </div>
    </div>
</x-app-layout>