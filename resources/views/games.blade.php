<x-app-layout>
    <div class="py-32">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-xl sm:rounded-lg">
                @livewire('game-page')
            </div>
        </div>
    </div>
</x-app-layout>