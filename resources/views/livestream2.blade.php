<x-app-layout>
    <div class="flex items-center justify-center w-screen h-screen pb-4 overflow-hidden pt-28 bg-white/90">
        <div class="w-full h-full aspect-video">
            <iframe class="w-full h-full" src="https://player.vimeo.com/video/711005551?h=1e3d3f67bf&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" title="Livestream" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</x-app-layout>