<x-app-layout>
    <div class="w-screen min-h-screen px-6 pb-4 bg-white pt-28 relative">
        <div class="h-full w-full flex flex-col items-center justify-center overflow-y-auto">
            <div class="w-full h-full border-4 border-white md:w-1/2">
                <img src="{{asset('img/rph.png')}}" class="w-full h-full">
            </div>
            <div class="w-full h-full border-4 border-white md:w-1/2">
                <img src="{{asset('img/hcps.png')}}" class="w-full h-full">
            </div>
        </div>
    </div>
</x-app-layout>