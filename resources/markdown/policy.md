# Privacy and Concent Policy

By providing your personal data under this form, you hereby consent to the processing of the same by Procter & Gamble (“P&G”) for the purpose of [Breaking the Wave: A Collaborative Virtual Conference] and that this will be retained by P&G until such purpose is accomplished.


You likewise consent that such data will be shared to P&G’s affiliates and third party service providers for the purpose of further processing of the same. 


For more information about how P&G processes your data as stated above, including the specific affiliates and third party service providers with whom your data may be shared by P&G for further processing, as well as what rights you have in relation to such data, please visit P&G’s privacy policy at https://www.pg.com/privacy/english/privacy_notice.shtml or you may contact +632 894 39 55.
