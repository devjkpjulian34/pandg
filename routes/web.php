<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group(['middleware' => ['auth:sanctum','verified']], function() {
    Route::resources([
        'users' => 'App\Http\Controllers\UserController',
    ]);

    Route::get('/cme', function () {
        return view('cme');
    })->name('cme');

    Route::get('/video', function () {
        return view('video');
    })->name('video');

    Route::get('/schedule', function () {
        return view('schedule');
    })->name('schedule');

    Route::get('/livestream/1', function () {
        return view('livestream1');
    })->name('livestream1');

    Route::get('/livestream/2', function () {
        return view('livestream2');
    })->name('livestream2');
    
    Route::get('/games', function () {
        return view('games');
    })->name('games');
    
    Route::get('/exhibit', function () {
        return view('exhibit');
    })->name('exhibit');

    Route::get('/exhibit/1', function () {
        return view('exhibit1');
    })->name('exhibit1');

    Route::get('/exhibit/2', function () {
        return view('exhibit2');
    })->name('exhibit2');

    Route::get('/exhibit/3', function () {
        return view('exhibit3');
    })->name('exhibit3');

    Route::get('/exhibit/4', function () {
        return view('exhibit4');
    })->name('exhibit4');

    Route::get('/exhibit/5', function () {
        return view('exhibit5');
    })->name('exhibit5');

    Route::get('/medhub', function () {
        return view('medhub');
    })->name('medhub');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
