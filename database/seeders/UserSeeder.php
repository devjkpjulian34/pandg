<?php

namespace Database\Seeders;

use Carbon\Carbon;

use App\Models\User;

use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            // [
            //     'name' => 'Super Admin',
            //     'email' => 'devjkpjulian@gmail.com',
            //     'prc_number' => 'Super Admin',
            //     'password' => Hash::make('Zxasqw12'),
            //     'email_verified_at' => Carbon::now(),
            //     'role' => 0
            // ],
            [
                'name' => 'Administrator',
                'email' => 'admin@pghealthmedicalevents.com',
                'prc_number' => '0000000',
                'password' => Hash::make('Zxasqw12'),
                'email_verified_at' => Carbon::now(),
                'role' => 0
            ],
            [
                'name' => 'TEST USER',
                'email' => 'test@pghealthmedicalevents.com',
                'prc_number' => 'test000000',
                'password' => Hash::make('Zxasqw12'),
                'email_verified_at' => Carbon::now(),
                'role' => 1
            ]
        );
        
        foreach ($users as $user) {
            User::create($user);
        }
    }
}
